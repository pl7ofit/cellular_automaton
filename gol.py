#!/usr/bin/env python3

from time import sleep as sleep
import random

neighbours = [(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
delay = 0.15
w=50
h=25
units_count=1
init_func = lambda : 0
#init_func = lambda : random.randint(0,1)

#game of life
#born_cells = lambda neig_n: neig_n in [3]
#dead_cells = lambda neig_n: neig_n not in [2,3]

#day and night
#born_cells = lambda neig_n: neig_n in [3,6,7,8]
#dead_cells = lambda neig_n: neig_n not in [3,4,6,7,8]

#custom
born_cells = lambda neig_n: neig_n in [1,4,6,8]
dead_cells = lambda neig_n: neig_n not in [2,3,5,7]


clear_console = lambda: print("\033[H\033[J", end='')
printfnx = lambda x: print("\033[F" * (x), end='')
printf = lambda s: print(s,end='')
printfn = lambda: print('')

def landing(xy, field, unit):
  field_size_x = len(field[0])
  field_size_y = len(field)
  unit_size_x = len(unit)
  unit_size_y = len(unit[0])
  for x in range(xy[1], xy[1]+unit_size_x):
    for y in range(xy[0], xy[0]+unit_size_y):
      field_x,field_y =  x%field_size_y, y%field_size_x
      unit_x, unit_y = x-xy[1], y-xy[0]
      #print('field: ',field_x, field_y)
      #print('unit: ',unit_x, unit_y)
      #print(unit)
      field[field_x][field_y]
      field[field_x][field_y] = unit[unit_x][unit_y]
  return field


glider1 = [[0,1,0],
          [0,0,1],
          [1,1,1]]

glider2 = [[0,1,1],
          [1,0,1],
          [0,0,1]]

glider3 = [[1,1,1],
          [1,0,0],
          [0,1,0]]

glider4 = [[1,0,0],
          [1,0,1],
          [1,1,0]]

gliders = [glider1,glider2,glider3,glider4]

stick = [[1,1,1]]
cross = [[0,1,0],
         [1,1,1],
         [0,1,0]]

pentamino = [[0,1,0],
            [0,1,1],
            [1,1,0]]

spaceship = [[0,0,0,1,0,0],
            [0,1,0,0,0,1],
            [1,0,0,0,0,0],
            [1,0,0,0,0,1],
            [1,1,1,1,1,0]]
pentadecatlon = [[1,1,1,1,1,1,1],
                [1,0,1,1,1,0,1],
                [1,1,1,1,1,1,1]]

block = [[1,1,1],
       [1,1,1]]

field = [[init_func() for string in range(w)] for height in range(h)]

for i in range(units_count):
#  field = landing([random.randint(0,w), random.randint(0,h)], field, random.choice(gliders))
#  field = landing([random.randint(0,w), random.randint(0,h)], field, random.choice([stick]))
#  field = landing([random.randint(0,w), random.randint(0,h)], field, random.choice([cross]))
#  field = landing([random.randint(0,w), random.randint(0,h)], field, random.choice([pentamino]))
#  field = landing([random.randint(0,w), random.randint(0,h)], field, random.choice([spaceship]))
#  field = landing([random.randint(0,w), random.randint(0,h)], field, random.choice([pentadecatlon]))
  field = landing([random.randint(0,w), random.randint(0,h)], field, random.choice([block]))

generation = 1
first_hash = '0b1'+''.join( [ ''.join([ str(i) for i in line]) for line in field] )
field_to_int = first_hash
hashes = list(range(0,32))
while 1:
  clear_console()
  for y in field:
    for cell in y:
      printf(cell)
    printfn()
  hashes[generation%32] = field_to_int
  cells_changes = []
  for y in range(h):
    for x in range(w):
      neighbours_n = 0
      for xy in neighbours:
        neighbours_n += field[(y+xy[1])%h][(x+xy[0])%w]
      if not field[y][x] and born_cells(neighbours_n):
        cells_changes.append((1,y,x))
      if field[y][x] and (dead_cells(neighbours_n)):
        cells_changes.append((0,y,x))
  for cell in cells_changes:
    field[cell[1]][cell[2]] = cell[0]
  field_to_int = '0b1'+''.join( [ ''.join([ str(i) for i in line]) for line in field] )
  generation += 1
  print('generation: ', generation )
  print( 'first_hash:   ', hex(int(first_hash,2)) )
  print( 'current_hash: ', hex(int(field_to_int,2)) )
  sleep(delay)
  if not cells_changes or field_to_int in hashes: break
